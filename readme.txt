=================================================  FLEX-BASEX README ===

Welcome to the source code of the Flex-BaseX search engine.

This is an extended version of the BaseX query engine (http://basex.org)
where the FleXy language and its BELOW and NEAR constraints have been
implemented.
This work has been performed by Emanuele Panzeri during his PhD
ressearch at the University of Milano-Bicocca.

FleXy is an extension of the XQuery/XPath 2.0 Full-Text language, where
approximate structural constraints allows users to explictly require an
approximate matching of structural constraints. A score is computed for
each matched element, such score represents the constaint satisfaction.

Further readings and publications:

E. Panzeri and G. Pasi. “Flexible structural constraints in
  XQuery Full-Text”. In: Proceedings of the 10th Conference on Open
  Research Areas in Information Retrieval (OAIR). Lisbon, Portugal, 2013,
  pp. 51–54.

E. Panzeri and G. Pasi. “Flex-BaseX: An XML engine with a flexible
  extension of XQuery Full-Text”. In: Proceedings of the 36th
  international ACM SIGIR conference on Research and development in
  Information Retrieval (SIGIR). Dublin, Ireland. 2013.
  
E. Panzeri, G. Pasi. “An Approach to Define Flexible Structural
  Constraints in XQuery”. In 8th International Conference of Active Media
  Technology (AMT 2012), Lecture Notes in Computer Science, Springer,
  VOLUME 7669, Macau, China, December 4-7, 2012, pp. 307-317.

For further details about installing and compiling the Flex-BaseX source
code, please refer to BaseX homepage (http://basex.org) and source code
repository (https://github.com/BaseXdb/basex).

========================================================================
