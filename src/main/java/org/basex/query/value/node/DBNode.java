package org.basex.query.value.node;

import static org.basex.query.QueryText.*;

import java.io.*;

import org.basex.build.*;
import org.basex.core.*;
import org.basex.data.*;
import org.basex.io.*;
import org.basex.query.*;
import org.basex.query.expr.*;
import org.basex.query.iter.*;
import org.basex.query.util.*;
import org.basex.query.value.*;
import org.basex.query.value.item.*;
import org.basex.query.value.type.*;
import org.basex.query.value.type.Type.ID;
import org.basex.query.var.*;
import org.basex.util.*;
import org.basex.util.ft.*;
import org.basex.util.hash.*;
import org.basex.util.list.*;

/**
 * Database nodes.
 *
 * @author BaseX Team 2005-12, BSD License
 * @author Christian Gruen
 */
public class DBNode extends ANode {
  /** Data reference. */
  public final Data data;
  /** Pre value. */
  public int pre;
  /** Namespaces. */
  private Atts nsp;

  /**
   * Constructor, creating a document node from the specified data reference.
   * @param d data reference
   */
  public DBNode(final Data d) {
    this(d, 0);
  }

  /**
   * Constructor, creating a node from the specified data reference.
   * @param d data reference
   * @param p pre value
   */
  public DBNode(final Data d, final int p) {
    this(d, p, d.kind(p));
  }

  /**
   * Constructor, specifying full node information.
   * @param d data reference
   * @param p pre value
   * @param k node kind
   */
  public DBNode(final Data d, final int p, final int k) {
    this(d, p, null, type(k));
  }

  /**
   * Constructor, specifying full node information.
   * @param d data reference
   * @param p pre value
   * @param r parent reference
   * @param t node type
   */
  DBNode(final Data d, final int p, final ANode r, final NodeType t) {
    super(t);
    data = d;
    pre = p;
    par = r;
  }

  /**
   * Constructor, specifying an XML input reference.
   * @param input input reference
   * @param prop database properties
   * @throws IOException I/O exception
   */
  public DBNode(final IO input, final Prop prop) throws IOException {
    this(Parser.xmlParser(input, prop));
  }

  /**
   * Constructor, specifying a parser reference.
   * @param parser parser
   * @throws IOException I/O exception
   */
  public DBNode(final Parser parser) throws IOException {
    this(MemBuilder.build("", parser));
  }

  /**
   * Sets the node type.
   * @param p pre value
   * @param k node kind
   */
  public final void set(final int p, final int k) {
    type = type(k);
    par = null;
    val = null;
    nsp = null;
    pre = p;
  }

  @Override
  public final Data data() {
    return data;
  }

  @Override
  public final byte[] string() {
    if(val == null) val = data.atom(pre);
    return val;
  }

  @Override
  public final long itr(final InputInfo ii) throws QueryException {
    final boolean txt = type == NodeType.TXT || type == NodeType.COM;
    if(txt || type == NodeType.ATT) {
      final long l = data.textItr(pre, txt);
      if(l != Long.MIN_VALUE) return l;
    }
    return Int.parse(data.atom(pre), ii);
  }

  @Override
  public final double dbl(final InputInfo ii) throws QueryException {
    final boolean txt = type == NodeType.TXT || type == NodeType.COM;
    if(txt || type == NodeType.ATT) {
      final double d = data.textDbl(pre, txt);
      if(!Double.isNaN(d)) return d;
    }
    return Dbl.parse(data.atom(pre), ii);
  }

  @Override
  public final byte[] name() {
    return type == NodeType.ELM || type == NodeType.ATT || type == NodeType.PI ?
      data.name(pre, kind(nodeType())) : Token.EMPTY;
  }

  @Override
  public final QNm qname() {
    return type == NodeType.ELM || type == NodeType.ATT || type == NodeType.PI ?
      qname(new QNm()) : null;
  }

  @Override
  public final QNm qname(final QNm name) {
    // update the name and uri strings in the specified QName
    final byte[] nm = name();
    byte[] uri = Token.EMPTY;
    final boolean pref = Token.indexOf(nm, ':') != -1;
    if(pref || data.nspaces.size() != 0) {
      final int n = pref ? data.nspaces.uri(nm, pre, data) :
        data.uri(pre, data.kind(pre));
      final byte[] u = n > 0 ? data.nspaces.uri(n) : pref ?
          NSGlobal.uri(Token.prefix(nm)) : null;
      if(u != null) uri = u;
    }
    name.set(nm, uri);
    return name;
  }

  @Override
  public final Atts namespaces() {
    if(type == NodeType.ELM && nsp == null) nsp = data.ns(pre);
    return nsp;
  }

  @Override
  public final byte[] baseURI() {
    if(type == NodeType.DOC) {
      final byte[] base = data.text(pre, true);
      if(data.inMemory()) {
        final String bs = Token.string(base);
        if(bs.endsWith(IO.BASEXSUFFIX + IO.XMLSUFFIX)) return Token.EMPTY;
        final String dir = data.meta.original;
        return Token.token(dir.isEmpty() ? bs : IO.get(dir).merge(bs).url());
      }
      return new TokenBuilder(data.meta.name).add('/').add(base).finish();
    }
    final byte[] b = attribute(new QNm(BASE, XMLURI));
    return b != null ? b : Token.EMPTY;
  }

  @Override
  public final boolean is(final ANode node) {
    return node == this || node instanceof DBNode &&
        data == node.data() && pre == ((DBNode) node).pre;
  }

  @Override
  public final int diff(final ANode node) {
    // compare fragment with database node; specify fragment first to save time
    if(node instanceof FNode) return -diff(node, this);
    // same database instance: compare pre values
    if(data == node.data()) {
      final int p = ((DBNode) node).pre;
      return pre > p ? 1 : pre < p ? -1 : 0;
    }
    // check order via lowest common ancestor
    return diff(this, node);
  }

  @Override
  public final DBNode copy() {
    final DBNode n = new DBNode(data, pre, par, nodeType());
    n.score = score;
    n.scoreStructure = scoreStructure;
    return n;
  }

  @Override
  public final Value copy(final QueryContext ctx, final VarScope scp,
      final IntObjMap<Var> vs) {
    return copy();
  }

  @Override
  public final DBNode dbCopy(final Prop prop) {
    final MemData md = data.inMemory() ? new MemData(data) : new MemData(prop);
    new DataBuilder(md).build(this);
    return new DBNode(md).parent(par);
  }

  @Override
  public final DBNode deepCopy() {
    return dbCopy(data.meta.prop);
  }

  @Override
  public final DBNode finish() {
    return copy();
  }

  @Override
  public final ANode parent() {
    if(par != null) return par;
    final int p = data.parent(pre, data.kind(pre));
    if(p == -1) return null;

    final DBNode node = copy();
    node.set(p, data.kind(p));
    node.score(Scoring.step(node.score()));
    // No flex-axis involved: the node inherits parent structural score
    // @TODO: assign a default score?
    return node;
  }

  @Override
  public final DBNode parent(final ANode p) {
    par = p;
    return this;
  }

  @Override
  public final boolean hasChildren() {
    final int k = data.kind(pre);
    return data.attSize(pre, k) != data.size(pre, k);
  }

  @Override
  public final AxisIter ancestor() {
    return new AxisIter() {
      private final DBNode node = copy();
      int p = pre;
      int k = data.kind(p);
      final double sc = node.score();
      final double scStructure = node.scoreStructure();

      @Override
      public ANode next() {
        p = data.parent(p, k);
        if(p == -1) return null;
        k = data.kind(p);
        node.set(p, k);
        node.score(Scoring.step(sc));
        // No flex-axis involved: the ancestor inherits parent structural score
        node.scoreStructure(scStructure);
        return node;
      }
    };
  }

  @Override
  public final AxisIter ancestorOrSelf() {
    return new AxisIter() {
      private final DBNode node = copy();
      int p = pre;
      int k = data.kind(p);
      final double sc = node.score();
      final double scStructure = node.scoreStructure();

      @Override
      public ANode next() {
        if(p == -1) return null;
        k = data.kind(p);
        node.set(p, k);
        node.score(Scoring.step(sc));
        // System.err.println("ancestor() Setting score: " + node.scoreStructure() + "; parent="+scStructure+"; node=" + node.toString());
        node.scoreStructure(scStructure);
        p = data.parent(p, k);
        return node;
      }
    };
  }

  @Override
  public final AxisMoreIter attributes() {
    return new AxisMoreIter() {
      final DBNode node = copy();
      final int s = pre + data.attSize(pre, data.kind(pre));
      int p = pre + 1;

      @Override
      public boolean more() {
        return p != s;
      }

      @Override
      public DBNode next() {
        if(!more()) return null;
        node.set(p++, Data.ATTR);
        return node;
      }
    };
  }

  @Override
  public final AxisMoreIter children() {
    return new AxisMoreIter() {
      int k = data.kind(pre);
      int p = pre + data.attSize(pre, k);
      final int s = pre + data.size(pre, k);
      final DBNode node = copy();
      final double sc = node.score();
      final double scStructure = node.scoreStructure(true);

      @Override
      public boolean more() {
        return p < s;
      }

      @Override
      public ANode next() {
        if(!more()) return null;
        k = data.kind(p);
        node.set(p, k);
        node.score(Scoring.step(sc));
        // No flex-axis involved: the children inherits parent structural score?
        node.scoreStructure(scStructure);
        p += data.size(p, k);
        return node;
      }
    };
  }

  @Override
  public final AxisIter descendant() {
    return new AxisIter() {
      int k = data.kind(pre);
      int p = pre + data.attSize(pre, k);
      final int s = pre + data.size(pre, k);
      final DBNode node = copy();
      final double sc = node.score();
      final double scStructure = node.scoreStructure(true);

      @Override
      public DBNode next() {
        if(p == s) return null;
        k = data.kind(p);
        node.set(p, k);
        p += data.attSize(p, k);
        node.score(Scoring.step(sc));
        // No flex-axis involved: the descendant inherits parent structural score
        node.scoreStructure(scStructure);
        return node;
      }
    };
  }

  @Override
  public final AxisIter descendantOrSelf() {
    return new AxisIter() {
      final DBNode node = copy();
      final int s = pre + data.size(pre, data.kind(pre));
      int p = pre;

      @Override
      public ANode next() {
        if(p == s) return null;
        final int k = data.kind(p);
        node.set(p, k);
        p += data.attSize(p, k);
        return node;
      }
    };
  }

  @Override
  public final AxisIter following() {
    return new AxisIter() {
      private final DBNode node = copy();
      final int s = data.meta.size;
      int k = data.kind(pre);
      int p = pre + data.size(pre, k);

      @Override
      public ANode next() {
        if(p == s) return null;
        k = data.kind(p);
        node.set(p, k);
        p += data.attSize(p, k);
        return node;
      }
    };
  }

  @Override
  public final AxisIter followingSibling() {
    return new AxisIter() {
      private final DBNode node = copy();
      int k = data.kind(pre);
      private final int pp = data.parent(pre, k);
      final int s = pp == -1 ? 0 : pp + data.size(pp, data.kind(pp));
      int p = pp == -1 ? 0 : pre + data.size(pre, k);

      @Override
      public ANode next() {
        if(p == s) return null;
        k = data.kind(p);
        node.set(p, k);
        p += data.size(p, k);
        return node;
      }
    };
  }

  @Override
  public final AxisIter parentIter() {
    return new AxisIter() {
      /** First call. */
      private boolean more;

      @Override
      public ANode next() {
        if(more) return null;
        more = true;
        return parent();
      }
    };
  }

  @Override
  public final AxisIter below() {
    return new AxisMoreIter() {
      int k = data.kind(pre);
      int p = pre + data.attSize(pre, k);
      private final int s = pre + data.size(pre, k);
      private final DBNode node = copy();
      private final double sc = node.score();
      private final double scStructure = node.scoreStructure();
      private final IntList stackNext = new IntList();
      private int size = 0;
      private int next = -1;
      
      @Override
      public boolean more() {
        //System.err.println("DBNode::below::more() p=" + p + "; s=" + s);
        return p != s;
      }

      @Override
      public ANode next() {
        if(!more()) {
          return null;
        }
        
        next = -1;
        while (!stackNext.isEmpty() &&  (next = stackNext.pop()) == p) {
          //System.err.println("DBNode::below::next() Pop-ed! p=" + p + " -- next=" + next + " -- stackSize=" + stackNext.size());
        }
        
        //System.err.println("DBNode::below::next() current next=" + next);
        if (next != -1 && next != p) {
          stackNext.push(next);
          //System.err.println("DBNode::below::next() Added next=" + (p+size) + ", stack size=" + stackNext.size());
        }
        //System.err.println("DBNode::below::next() next=" + next + " stack.size=" + stackNext.size());

        
        k = data.kind(p);
        node.set(p, k);
        size = data.size(p,k);

        node.score(Scoring.step(sc));
        
        // Only "Element" nodes can be scored for structural constraints?
        //if (node.nodeType().equals(NodeType.ELM)) {
          /*
          System.err.println("DBNode::below::next() Element ----------------------------------------------------");
          System.err.println("DBNode::below::next() Node = " + node.toString());
          System.err.println("DBNode::below::next() pre=" + p + "; size=" + size + " dist=" + data.dist(p, k));
          System.err.println("DBNode::below::next() stackSize = " + stackNext.size());

          //*/
          node.scoreStructure(
            ScoringStructure.mergeStep(scStructure, ScoringStructure.descendingStep(stackNext.size()+1))
          );
          //System.err.println("DBNode::below::next() score: " + node.scoreStructure());
        //}
        
        stackNext.push(p + size);
        //System.err.println("DBNode::below::next() Added next=" + (p+size) + ", stack size=" + stackNext.size());
        p += data.attSize(p, k);
        return node;
      }
    };
  }

  @Override
  public final AxisIter above() {
    return new AxisIter() {
      private final DBNode node = copy();
      int p = pre;
      int k = data.kind(p);
      final double sc = node.score();
      final double scStructure = node.scoreStructure();
      private short steps = 0;

      @Override
      public ANode next() {
        p = data.parent(p, k);
        if(p == -1) return null;
        k = data.kind(p);
        node.set(p, k);
        node.score(Scoring.step(sc));
        // @todo: EMA, use merging of previous structural scores?
        node.scoreStructure(ScoringStructure.ascendingStep(++steps));
        return node;
      }
    };
  }
  
  @Override
  public final AxisIter aboveLimited(final short threshold) {
    return new AxisIter() {
      private final DBNode node = copy();
      int p = pre;
      int k = data.kind(p);
      final double sc = node.score();
      final double scStructure = node.scoreStructure();
      private short steps = 0;

      @Override
      public ANode next() {
        p = data.parent(p, k);
        if(p == -1 || steps >= threshold) return null;
        k = data.kind(p);
        node.set(p, k);
        node.score(Scoring.step(sc));
        // @todo: EMA, use merging of previous structural scores?
        node.scoreStructure(ScoringStructure.ascendingStep(++steps));
        return node;
      }
    };
  }
  
  @Override
  public AxisMoreIter belowLimited(final short threshold) {
    return belowLimited(threshold, 0);
  }
  
  
  private AxisMoreIter belowLimited(final int threshold, final int distance) {
      return belowLimited(threshold, distance, -1);
  }
  
  /**
   * Helper fnction fof NEAR axis evaluation
   * @param node the context node
   * @param level the contenx node initial distance
   * @param threshold max deep distance to iterate
   * @return 
   */
  private AxisMoreIter belowLimited(final int iThreshold, final int iDistance, final int skipNode) {
    return new AxisMoreIter() {
      int k = data.kind(pre);
      int p = pre + data.attSize(pre, k);
      final int s = pre + data.size(pre, k);
      final int skipPre = skipNode;
      final int threshold = iThreshold;
      final int distance = iDistance;
      private final DBNode node = copy();
      private final double sc = node.score();
      // private final double scStructure = node.scoreStructure();
      //private final ArrayListStack<Integer> stackNext = new ArrayListStack<Integer>();
      private final IntList stackNext = new IntList();
      private int size = 0;
      private int next = -1;
      
      @Override
      public boolean more() {
        //System.err.println("DBNode::belowLimited::more() pre=" + p + "; s=" + s);
        //System.err.println("DBNode::belowLimited::more() distance=" + distance + "; threshold=" + threshold);
        if (distance >= threshold) {
            return false;
        }
        // Skip the given subtree
        if (p == skipPre) {
            //System.err.println("DBNode::belowLimited::more() skipping the subtree p=" + skipPre);
            k = data.kind(p);
            p += data.size(p, k);
            //System.err.println("DBNode::belowLimited::more() moving to p=" + p);
        }
        return p != s;
      }
      
      /**
       * Flush the node stack untill we reach the given end PRE identifier
       * @param end, the pre-identifier to end at
       */
      private int flushStack(final int end) {
        int next = -1;
        while (!stackNext.isEmpty() &&  (next = stackNext.pop()) == end) {
          // If this node is the end of the previuos one, pop the latest End
          //System.err.println("flushStack() Pop-ed! p=" + end + "; curr_end=" + next + "; stackSize=" + stackNext.size());
        }
        return next;
      }

      @Override
      public ANode next() {
        if(!more()) {
          //System.err.println("DBNode::belowLimited::next() No more items, exit!");
          return null;
        }
        
        next = -1;
        while (!stackNext.isEmpty() &&  (next = stackNext.pop()) == p) {
          // If this node is the end of the previuos one, pop the latest End
          //System.err.println("flushStack() Pop-ed! p=" + end + "; curr_end=" + next + "; stackSize=" + stackNext.size());
        }
        
        // int next = flushStack(p);
        if (next != -1 && next != p) {
          // Store latest node end
          // System.err.println("UnPoping next=" + next);
          stackNext.push(next);
        }
        k = data.kind(p);
        node.set(p, k);
        size = data.size(p,k);
        node.score(Scoring.step(sc));
        
        // System.err.println("* Element ------------ pre="+p+"; size="+size);
        
        // Only "Element" nodes can be scored for structural constraints
        //if (node.nodeType().equals(NodeType.ELM)) {
          /*
          System.err.println("* Node=" + node + " isElement=" + (node.nodeType() == NodeType.ELM));
          System.err.println("* Depth=" + (stackNext.size() +1) + ",score=" + ScoringStructure.descendingStepLimited(stackNext.size()+1 + distance, threshold));
          //*/
          // Apply the limited descending scoring
          node.scoreStructure(ScoringStructure.descendingStepLimited(stackNext.size()+1 + distance, threshold));
          //node.scoreStructure(ScoringStructure.descendingStep(stack_ends.size()+1));
          // System.err.println("BelowLimited: score=" + node.scoreStructure() +";node=" + node.toString());
        /*}
        else {
          // System.err.println("Skipping NON ELEMENT node");
        }
        */ 
        
        stackNext.push(p + size);
        //System.err.println("PushingEnd() next-end:" + (p+size) + ";stackSize=" + stackNext.size());
        p += data.attSize(p, k);
        
        //System.err.println("belowLimited::next() dist=" + (stackNext.size() + distance) + ";max=" + threshold);
        if (/*node.nodeType().equals(NodeType.ELM) && */ stackNext.size() + distance >= threshold) {
          // If we passed the max allowed distance, skip this node and its contents
          p = stackNext.pop(); // flushStack(current_end);
          //System.err.println("belowLimited::next() Over the MAX distance, fall back to parent? p =" + p);
        }
        //System.err.println("Final: current_end=" + next + ";stackSize=" + stackNext.size());
       
        return node;
      }
    };
  }
  
  @Override
  public final AxisIter near(final short n) {
    return new AxisIter() {
      // private int tot = 0;
      private final short threshold = n;
      private short backward = 0;
      // private ArrayList<Integer> list = new ArrayList<Integer>();
      private IntList list = new IntList();
      
      private final DBNode contextNode = copy();
      private DBNode currentNode;
      
      // At the beginning, start with 
      private AxisMoreIter iterator = belowLimited(threshold);
      private boolean doBackstep = false;
      
      // int p = pre;
      // int k = data.kind(p);
      private final double sc = contextNode.score();
      // private final double scStructure = context_node.scoreStructure();

      @Override
      public ANode next() {
        currentNode = getNextNode();
        if (currentNode == null && doBackstep == false){
            currentNode = getNextNode();
        }
        
        // Skip duplicated nodes that can occur during ancestor step evaluation
        while (currentNode != null && list.contains(currentNode.pre)) {
          //System.err.println("DBNode:near::next() skipping already visited node=" + currentNode.toString());
          currentNode = getNextNode();
        }

        if (currentNode != null) {
          list.add(currentNode.pre);
          currentNode.score(Scoring.step(sc));
          
          // If we reached the maximum distance we're in the laste self:* step
          if (doBackstep) {
            //System.err.println("DBNode:near::next() setting node weight due to BackStep");
            // Setting the node score accordingly
            currentNode.scoreStructure(ScoringStructure.descendingStepLimited(backward, threshold));
          }
          //System.err.println("DBNode:near::next() currentNode="+ currentNode.toString() +";score-st=" + currentNode.scoreStructure);
        }
        /*
        else {
          System.err.println("Tot=" + tot);
        }
        */
        return currentNode;
      }

      /**
       * Get next iterator: recursively going back to parents and ancestors..
       */
      private DBNode getNextNode() {
        //System.err.println("DBNode:near::getNextNode() backstep=" + doBackstep + "; threshold=" + threshold);
        // tot++;
        if (!iterator.more()) {
          if (doBackstep) {
            // If we performed a backstep (so not a real backstep), reset the backward variable
            backward--;
            //System.err.println("DBNode:near::getNextNode() backward decreased");
          }
          if (++backward <= threshold) {
            // If the current iterator is over, go to the next parent one
            DBNode temp = copy();
            int i = 0;
            int prevPre = temp.pre;
            // System.err.println("DBNode:near::getNextNode() c.pre=" + currentNode.pre +"; c.size=" + currentNode.data.size(currentNode.pre, currentNode.kind()));
            for (; i < backward && temp.parent() != null; i++) {
              prevPre = temp.pre;
              //System.err.println("DBNode:near::getNextNode() backward=" + backward + "; i=" +i + "; prevPre=" + prevPre);
              temp = (DBNode)temp.parent();
              //System.err.println("DBNode:near::getNextNode() temp.pre=" + temp.pre +"; temp.size=" + temp.data.size(temp.pre, temp.kind()));
            }
            if (temp.pre == 0) {
                return null;
            }
            if (i == backward) {
              // We identify the next ancestor to iterate from
              if (doBackstep == false) {
                // The maximum distance is the TEMP node itself, its descendants will have a distance of max +1!
                //System.err.println("DBNode:near::getNextNode() Continue with self() max=" + threshold + ";currenteDistance=" + backward + ";node=" + temp.toString());
                iterator = temp.self();
                doBackstep = true;
              }
              else {
                // Iterate over the TEMP node descendants
                //System.err.println("DBNode:near::getNextNode() Continue with belowLimited() max=" + threshold+ "; backward=" + backward + "; skipPre=" + prevPre+ "; currentNode=" + temp.toString());
                iterator = temp.belowLimited(threshold, backward, prevPre);
                // Next iterator will return the SELF iterator!
                doBackstep = false;
              }
            }
            else {
              // There's no more parents: let the iterator return NULL during next() invocation
              //System.err.println("DBNode:near::getNextNode() cant't go any further, parent at level" + backward +" is null!");              
            }
          }
          else {
            // We go further the max distance, let the iterator return NULL during next() invocation
            //System.err.println("DBNode:near::getNextNode() backward=" + backward +" > distance=" + threshold + " : we're finished!");
          }
        }
        // Finally return the next node!
        return (DBNode) iterator.next();
      }
    };
  }

  @Override
  public final boolean sameAs(final Expr cmp) {
    return cmp instanceof DBNode && data == ((DBNode) cmp).data &&
      pre == ((DBNode) cmp).pre;
  }

  @Override
  public final void plan(final FElem plan) {
    addPlan(plan, planElem(NAM, data.meta.name, PRE, pre));
  }

  @Override
  public final byte[] xdmInfo() {
    final ByteList bl = new ByteList().add(typeId().asByte());
    if(type == NodeType.DOC) bl.add(baseURI()).add(0);
    else if(type == NodeType.ATT) bl.add(qname().uri()).add(0);
    return bl.toArray();
  }

  @Override
  public final ID typeId() {
    // check if a document has a single element as child
    Type.ID t = type.id();
    if(type == NodeType.DOC) {
      final AxisMoreIter ai = children();
      if(ai.more() && ai.next().type == NodeType.ELM && !ai.more()) t = NodeType.DEL.id();
    }
    return t;
  }

  @Override
  public String toString() {
    final TokenBuilder tb = new TokenBuilder(type.string()).add(' ');
    switch((NodeType) type) {
      case ATT:
      case PI:
        tb.add(name()).add(" { \"").add(Token.chop(string(), 64)).add("\" }");
        break;
      case ELM:
        tb.add(name()).add(" { ... }");
        break;
      case DOC:
        tb.add("{ \"").add(data.text(pre, true)).add("\" }");
        break;
      default:
        tb.add("{ \"").add(Token.chop(string(), 64)).add("\" }");
        break;
    }
    return tb.toString();
  }
}
