package org.basex.query.path;

import org.basex.query.iter.*;
import org.basex.query.value.node.*;

/**
 * XPath axes.
 *
 * @author BaseX Team 2005-12, BSD License
 * @author Christian Gruen
 */
public enum Axis {
  // ...order is important here for parsing the Query;
  // axes with longer names are parsed first

  /** Ancestor-or-self axis. */
  ANCORSELF("ancestor-or-self", false) {
    @Override
    AxisIter iter(final ANode n) {
      return n.ancestorOrSelf();
    }
  },

  /** Ancestor axis. */
  ANC("ancestor", false) {
    @Override
    AxisIter iter(final ANode n) {
      return n.ancestor();
    }
  },

  /** Attribute axis. */
  ATTR("attribute", true) {
    @Override
    AxisIter iter(final ANode n) {
      return n.attributes();
    }
  },

  /** Child Axis. */
  CHILD("child", true) {
    @Override
    AxisIter iter(final ANode n) {
      return n.children();
    }
  },

  /** Descendant-or-self axis. */
  DESCORSELF("descendant-or-self", true) {
    @Override
    AxisIter iter(final ANode n) {
      return n.descendantOrSelf();
    }
  },

  /** Descendant axis. */
  DESC("descendant", true) {
    @Override
    AxisIter iter(final ANode n) {
      return n.descendant();
    }
  },

  /** Following-Sibling axis. */
  FOLLSIBL("following-sibling", false) {
    @Override
    AxisIter iter(final ANode n) {
      return n.followingSibling();
    }
  },

  /** Following axis. */
  FOLL("following", false) {
    @Override
    AxisIter iter(final ANode n) {
      return n.following();
    }
  },

  /** Parent axis. */
  PARENT("parent", true) {
    @Override
    AxisIter iter(final ANode n) {
      return n.parentIter();
    }
  },

  /** Preceding-Sibling axis. */
  PRECSIBL("preceding-sibling", false) {
    @Override
    AxisIter iter(final ANode n) {
      return n.precedingSibling();
    }
  },

  /** Preceding axis. */
  PREC("preceding", false) {
    @Override
    AxisIter iter(final ANode n) {
      return n.preceding();
    }
  },

  /** Below axis. */
  BELOW("below", true) {
    @Override
    AxisIter iter(final ANode n) {
      return n.below();
    }
  },
  
  /** Below2 axis. */
  BELOW2("below2", true) {
    @Override
    AxisIter iter(final ANode n) {
      return n.belowLimited((short)2);
    }
  },
  
  /** Below3 axis. */
  BELOW3("below3", true) {
    @Override
    AxisIter iter(final ANode n) {
      return n.belowLimited((short)3);
    }
  },
  
  /** Below4 axis. */
  BELOW4("below4", true) {
    @Override
    AxisIter iter(final ANode n) {
      return n.belowLimited((short)4);
    }
  },
  
  /** Below5 axis. */
  BELOW5("below5", true) {
    @Override
    AxisIter iter(final ANode n) {
      return n.belowLimited((short)5);
    }
  },  

  /** Near axis. */
  NEAR("near", false) {
    @Override
    AxisIter iter(final ANode n) {
      return n.near((short)1);
    }  
  },
  /** Near2 axis. */
  NEAR2("near2", false) {
    @Override
    AxisIter iter(final ANode n) {
      return n.near((short)2);
    }  
  },
  
  /** Near3 axis. */
  NEAR3("near3", false) {
    @Override
    AxisIter iter(final ANode n) {
      return n.near((short)3);
    }  
  },
  
    /** Near4 axis. */
  NEAR4("near4", false) {
    @Override
    AxisIter iter(final ANode n) {
      return n.near((short)4);
    }  
  },

  /** Above axis. */
  ABOVE("above", false) {
    @Override
    AxisIter iter(final ANode n) {
      return n.above();
    }  
  },

  /** Above3 axis. */
  ABOVE3("above3", false) {
    @Override
    AxisIter iter(final ANode n) {
      return n.aboveLimited((short)3);
    }  
  },

  /** Step axis. */
  SELF("self", true) {
    @Override
    AxisIter iter(final ANode n) {
      return n.self();
    }
  };

  /** Cached enums (faster). */
  public static final Axis[] VALUES = values();
  /** Axis string. */
  public final String name;
  /** Descendant axis flag. */
  public final boolean down;

  /**
   * Constructor.
   * @param n axis string
   * @param d descendant flag
   */
  Axis(final String n, final boolean d) {
    name = n;
    down = d;
  }

  /**
   * Returns a node iterator.
   * @param n input node
   * @return node iterator
   */
  abstract AxisIter iter(final ANode n);

  @Override
  public String toString() {
    return name;
  }

  /**
   * Inverts the axis.
   * @return inverted axis
   */
  final Axis invert() {
    switch(this) {
      case ANC:        return DESC;
      case ANCORSELF:  return DESCORSELF;
      case ATTR:
      case CHILD:      return PARENT;
      case DESC:       return ANC;
      case DESCORSELF: return ANCORSELF;
      case FOLLSIBL:   return PRECSIBL;
      case FOLL:       return PREC;
      case PARENT:     return CHILD;
      case PRECSIBL:   return FOLLSIBL;
      case PREC:       return FOLL;
      case SELF:       return SELF;
      // Flexible axis inversion
      case BELOW:      return ABOVE;
      case ABOVE:      return BELOW;
      case BELOW3:     return ABOVE3;
      case ABOVE3:     return BELOW3;
      default:         return null;
    }
  }
}
