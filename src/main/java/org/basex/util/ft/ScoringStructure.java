package org.basex.util.ft;

/**
 * Default Structure scoring model, assembling all score calculations.
 *
 * @author Emanuele Panzeri
 */
public final class ScoringStructure {

  public static double let(double sum, int sz) {
      return sum;
  }

  /** Private constructor. */
  private ScoringStructure() { }

  /**
   * Returns the score for the given descending steps
   * @param steps
   * @return 
   */
  public static double descendingStep(final int steps) {
    return (steps > 0 ? 1.0/steps : 1.0);
  }
  
  /**
   * Returns the score for the given ascending steps
   * @todo: check his function
   * @param steps
   * @return 
   */
  public static double ascendingStep(final int steps) {
    return (steps > 0 ? 1.0/steps : 1.0);
  }

  /**
   * Returns the NEAR scoring for the given steps and max-distance
   * @param steps
   * @param max
   * @return 
   */
  public static double nearStep(final int steps, final int max) {
    if (steps > max || steps < 0)
      return 0;
    else {
      return 1.0 - (((double)steps - 1)/(double)max);
    }
  }
  
  /**
   * Merge two structural scores during a path traversal (MIN value)
   * @param sc1
   * @param sc2
   * @return 
   */
  public static double mergeStep(final double sc1, final double sc2) {
    if (sc1 == -1 && sc2 != -1)
      return sc2;
    if (sc1 != -1 && sc2 == -1)
      return sc1;
    // System.err.println("merge() score1=" + sc1 + ";score2=" + sc2 + "; finel=" + ((sc1 + sc2) / 2));
    return Math.min(sc1, sc2);
  }
  
  /**
   * Merge two structural scores during a path branching merge (MAX value)
   * @param sc1
   * @param sc2
   * @return 
   */
  public static double mergePredicates(final double sc1, final double sc2) {
    // System.err.println("merge Predicates");
    if (sc1 == -1 && sc2 != -1)
      return sc2;
    if (sc1 != -1 && sc2 == -1)
      return sc1;
      
    // System.err.println("mergePredicates() score1=" + sc1 + ";score2=" + sc2 + "; finel=" + ((sc1 + sc2) / 2));
    return Math.max(sc1, sc2);
  }
}
